const { Content } = require('../model/Content');
const { successResponse, failedResponse } = require('../helpers/response');

getAll = async (req, res) => {
  const content = await Content.find().populate({
    path: "_user",
    select: "name photo"
  })
  try {
    if (content) {
      return res.status(200).json(successResponse(content))
    }
  } catch (error) {
    return res.status(400).json(failedResponse(error))
  }
}

addContent = async (req, res) => {
  try {
    const { title, body, category, location } = req.body;
    const content = new Content({
      title,
      body,
      category,
      location,
      _user: req.user._id
    })

    await content.save();
    res.status(200).json(successResponse(content));
  } catch (error) {
    res.status(400).json(failedResponse(error));
  }
}

delContent = async (req, res) => {
  let content = await Content.deleteOne({
    _id: req.params.id,
    _user: req.user._id
  });
  if (!content) {
    return res.status(404).json(failedResponse('Invalid content'));
  }
  res.status(200).json(successResponse(content, 'Content deleted'));
}

updateContent = async (req, res) => {
  const content = await Content.findByIdAndUpdate({
    _id: req.params.id,
    _user:req.user._id
  }, 
  {
    $set: req.body
  },
  {
    new: true
  });

  if (!content) {
    return res.status(404).json(failedResponse('Invalid content'));
  }
  res.status(200).json(successResponse(content));
}



module.exports = {
  getAll,
  addContent,
  delContent,
  updateContent
}