const { User } = require('../model/User');
const { successResponse, failedResponse } = require('../helpers/response');
const bcrypt = require('bcrypt');
const multer = require('multer');
const cloudinary = require('cloudinary');
const datauri = require('datauri');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

getAll = async (req, res) => {
  let user = await User.find()
    .select('-password -__v')
    .exec()
  try {
    if (user) {
      res.status(200).json(successResponse(user));
    }
  } catch (error) {
    res.status(500).json(failedResponse(error));
  }
}

addUser = async (req, res) => {
  let { name, email, password, gender, interest, about, location } = req.body;
  let user = await User.findOne({ email });
  if (user) {
    return res.status(409).json(failedResponse('Email is exist'));
  }
  user = new User({
    name,
    email,
    password,
    gender,
    interest,
    about,
    location
  });
  try {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    const token = crypto.randomBytes(32).toString('hex');
    user.token = token;
    user.tokenExpiration = Date.now() + 3600000;
    await user.save();
    res.status(201).json(successResponse(user));
  } catch (error) {
    return res.status(400).json(failedResponse(error));
  }
}

delUser = async (req, res) => {
  const user = await User.deleteOne({ _id: req.params.id });
  if (!user) {
    return res.status(404).json(failedResponse('Invalid user'))
  }
  res.status(200).json(successResponse(user, 'User deleted'))
}

updateUser = async (req, res) => {
  let user = await User.findByIdAndUpdate(req.user._id, { $set: req.body }, { new: true });

  if (!user) {
    return res.status(404).json(failedResponse('Invalid user'));
  }
  res.status(200).json({ data: user });
}

signIn = async (req, res, next) => {
  const { password, email } = req.body;
  const userExist = await User.findOne({ email });

  if (!userExist) {
    res.status(404).json(failedResponse('Email incorrect'));
  } else if (!password || password === "" || password === null) {
    res.status(400).json(failedResponse('Password must be filled'));
  }

  bcrypt.compare(password, userExist.password)
  .then(result => {
    if (result) {
      const token = jwt.sign(userExist.toJSON(), process.env.JWT_KEY, {
        algorithm: 'HS256',
        expiresIn: '12h'
      })
      res.setHeader('Authorization', token)

      return res.status(200).json(successResponse(token, 'Login success'));
    } else {
      return res.status(401).json(failedResponse('Password incorrect'));
    }
  })
  .catch(err => {
    next(err)
  })
}

getProfile = async (req, res) => {
  let user = await User.findById(req.user._id).select('-password');

  if (!user) {
    return res.status(404).json(failedResponse('Invalid user'));
  }
  res.status(200).json(successResponse(user));
}

module.exports = {
  getAll,
  addUser,
  delUser,
  updateUser,
  signIn,
  getProfile
};