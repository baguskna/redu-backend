const express = require('express');
const router = express.Router();
const user = require('./userRouters');
const content = require('./ContentRouters');

router.use('/users', user);
router.use('/contents', content);

module.exports = router;
