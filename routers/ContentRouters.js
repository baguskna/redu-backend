const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const objectId = require('../middleware/object-id');
const { getAll, addContent, delContent, updateContent } =require('../controller/ContentController');

router.get("/", getAll);
router.post("/", auth, addContent);
router.delete("/delete/:id", auth, objectId, delContent);
router.put("/update/:id", auth, objectId, updateContent);

module.exports = router;
