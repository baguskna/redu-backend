const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const objectId = require('../middleware/object-id');
const { getAll, addUser, delUser, updateUser, signIn, getProfile } = require('../controller/userController');

router.get('/', getAll);
router.post('/', addUser);
router.delete('/:id', delUser);
router.put('/:id', auth, updateUser);
router.post('/login', signIn);
router.get('/:id', auth, getProfile);

module.exports = router;
