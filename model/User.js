const mongoose = require('mongoose');
require('mongoose-type-email');
const uniqueValidator = require('mongoose-unique-validator');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
  email: {
    type: mongoose.SchemaTypes.Email,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    enum: ["male", "female", "other"]
  },
  about: {
    type: String
  },
  interest: [String],
  isAdmin: {
    type: Boolean,
    default: false
  },
  token: String,
  tokenExpiration: Date,
  isVerified: {
    type: Boolean,
    default: false
  },
  photo: {
    public_id: String,
    secure_url: String
  },
  location: {
    type: String
  }
});

userSchema.plugin(uniqueValidator);

userSchema.methods.generateToken = () => {
  const token = jwt.sign(
    {
      _id: this._id,
      isAdmin: this.isAdmin,
      isVerified: this.isVerified
    },
    process.env.JWT_KEY
  );
  return token;
}

const User = new mongoose.model('users', userSchema);

exports.User = User;
