const mongoose = require('mongoose');

const contentSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  body: {
    type: String,
    required: true
  },
  category: {
    type: String,
    lowercase: true,
    required: true
  },
  location: {
    type: String,
    lowercase: true,
    required: true
  },
  _user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  // photo: {
  //   public_id: String,
  //   secure_url: String
  // }
})

const Content = new mongoose.model("contents", contentSchema);

exports.Content = Content;
