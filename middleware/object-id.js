const mongoose = require('mongoose');
const { failedResponse } = require('../helpers/response');

module.exports = objectId = (req, res, next) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).json(failedResponse('Invalid id'));
  }
  next();
}
