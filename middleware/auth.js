const { User } = require('../model/User');
const { failedResponse } = require('../helpers/response');
const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
  let token = req.header('Authorization');
  if (!token) {
    return res.status(401).json(failedResponse('You have not logined yet'))
  }

  let tokenSplit = token.split(' ');

  try {
    let decode = jwt.verify(tokenSplit[1], process.env.JWT_KEY);
    req.user = decode;
     let user = await User.findById(req.user._id);
     if (!user) {
       return res.status(404).json(failedResponse('User does not exist'))
     }
     next();
  } catch (error) {
    res.status(422).json(failedResponse('Invalid token'));
  }
}
