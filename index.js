const express = require('express');
const app = express();
const mongoose = require('mongoose');
require('dotenv').config();
const router = require('./routers');

mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });
const db = mongoose.connection
db.on('error', error => console.log(error));
db.once('open', () => console.log('Connected To DataBase'));

app.use(express.json());

app.get('/', (req, res) => {
  res.send('REDU');
})

app.use('/api', router);

const port = 5000;
app.listen(port, () => console.log(`listening in port ${port}`));

module.exports = app;
