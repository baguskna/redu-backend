const successResponse = (results, message) => {
  return {
    success: true,
    message,
    results
  }
}

const failedResponse = (results, message) => {
  return {
    success: false,
    message,
    results
  }
}

module.exports = { successResponse, failedResponse };
